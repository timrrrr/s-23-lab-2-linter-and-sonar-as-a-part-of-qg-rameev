# Lab 2 -- Linter and SonarQube as a part of quality gates

## SonarQube

![](./img/SonarQ.jpeg)


## SonarCloud
When runing SonarCloud locally from the project folder it works fine:
![](./img/SonarCloud.jpeg)



I could not verify gitlab account identity so decided to use runner locally from my m1 mac machine

After installing gitlab runner manually folowing official docs (https://docs.gitlab.com/runner/install/osx.html#homebrew-installation-alternative)

There is a problem with ARM mac machines: error MacOS: LaunchAgent - Service could not initialize on M1 architecture (https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28136)



➜  gitlab-runner status
Runtime 

platform                                    arch=amd64 os=darwin pid=7340 revision=8925d9a0 version=14.1.0
gitlab-runner: Service has stopped

Where it should say Service is running


Homebrew installation also did not work

So I could not use my local machine as a runner